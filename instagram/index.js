const Instagram = require('instagram-web-api')
const Post = require('../instagram/model/post')
const SendPosts = require('../telegram/index')

const client = new Instagram({ username:'kiriltest22', password:'12111997Dd' });

const UserLogin = async () => {
    await client.login()
    return true
}

const getProfile = async () => {
    let profile = await client.getProfile()
    if(profile==undefined){
        await UserLogin()
        profile = await client.getProfile()
    }
    return 'forexianet'
}

const GetPhotos = async () => {
    let username = await getProfile();
    const photo = await client.getPhotosByUsername({ username: username, first: 5 })

    return photo
}

const SavePhoto = async (type) => {
    try {
        const photos = await GetPhotos();
        for(const photo of photos.user.edge_owner_to_timeline_media.edges){
            const url = photo.node.display_url
            const dbPost = await Post.findOne({ url });
            if(!dbPost){
                const post = new Post({
                        url: url
                })
                const data = await post.save()
                if(type!='init'){
                    await SendPosts(url)
                }
                console.log(url)
            }
        }
        return photos
    } catch(err){
        return err
    }
}

const InstagramController = {
    login : UserLogin,
    photos: GetPhotos,
    savePhoto: SavePhoto
}

module.exports = InstagramController