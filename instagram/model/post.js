const mongoose = require('mongoose');

const PostSchema = mongoose.Schema({
    url:  String,
});

const Post = mongoose.model('Post', PostSchema, 'posts');


module.exports = Post
