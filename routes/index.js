var express = require('express');
var router = express.Router();
const InstagramController = require('../instagram')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/getPhotos', async function(req, res, next) {
  await InstagramController.savePhoto('typical')
  res.render('index', { title: 'Express' });
});

router.get('/init', async function(req, res, next) {
  await InstagramController.savePhoto('init')
  res.render('index', { title: 'Express' });
});

module.exports = router;
